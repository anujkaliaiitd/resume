High performance networked systems are an important component of
modern datacenters.  Recent years have seen an industry-wide transition from
1 Gbps Ethernet to 10 Gbps Ethernet, and 40+ Gbps networks with advanced features
like Remote Direct Memory Access (RDMA) are currently being deployed.  The major
goal of my research is to design software architectures and communication
protocols that enable efficient utilization of this increasingly fast network
hardware.

A central theme of my research is understanding the performance
characteristics of a system's hardware components, and then using
this information for devising software that provides the best performance
to an application. My first contribution is HERD, an in-memory key-value store that
uses a hybrid communication protocol to deliver the maximum possible performance of
an RDMA network. My second contribution is a compiler framework, called G-Opt, that
automatically hides the DRAM latency of CPU-based packet-processing code and
outperforms GPU-offloaded implementations of several applications.

\section*{HERD: Using RDMA Efficiently for Key-Value Services~\cite{Kalia:2014}}
In-memory key-value stores are an important building block of large-scale web
services.  For example, Facebook's Memcache deployment~\cite{Nishtala:2013} consists of thousands of
machines and acts as an object cache for trillions of data items.  Operations
per second (Op/s) and end-to-end latency are among the primary performance
metrics of a key-value store. These metrics can benefit significantly from
the use of advanced network features like RDMA. Driven by the increasing demand
for faster networks, RDMA-capable networks like InfiniBand have witnessed a
resurgence in the past few years.  In fact, RDMA-capable hardware (switches 
and NICs) is cost-competitive with plain Ethernet hardware in the 10+ Gbps range.

Initial exploration of using RDMA to build key-value storage systems relied
heavily on RDMA read operations~\cite{Dragojevic:2014, Mitchell:2013}.
In an RDMA read-based system, the key-value
data structure is stored in the servers' DRAM and is accessed by the clients
using RDMA reads. Apart from being very intuitive, this approach seemed
attractive because key-value GET operations bypassed the servers' CPU.  However,
it required at least two round trips per GET operation due to the inherent
indirection in indexing data structures.  In HERD, I performed an exhaustive analysis
of RDMA operations and discovered that RDMA writes, being $\frac{1}{2}$~RTT operations, offer
higher performance than RDMA reads.  Further, contrary to prior belief, I showed
that RDMA's scalable datagram transport could be used without degrading performance.
With these observations, I designed a general-purpose, scalable request-reply mechanism that
delivers throughput close to native RDMA reads. It enables the HERD key-value
store to deliver up to 2X higher throughput and 50\% lower latency than existing
RDMA-based key-value stores.  Further, it can be used to implement new RDMA
primitives such as pointer-dereferencing RDMA reads that are not currently
supported by NIC hardware.

\section*{G-Opt: Raising the Bar for Using GPUs in Software Packet Processing~\cite{Kalia:2015}}
Software-based packet processing offers flexibility to network functions
such as routing, deep packet inspection, and access control.  This flexibility
is important for providing more sophisticated functionality than offered
by available hardware, and for supporting innovation of new network protocols.
Delivering high performance in software-based packet processing is a key concern
and has been the focus of several recent research projects.  One common technique
that has been adopted by many systems, most prominently PacketShader~\cite{Han:2010},
is the use of Graphical Processing Units
(GPUs) as accelerators for packet processing.  GPUs provide an order of magnitude higher
computation power than modern multi-core CPUs, and can effectively hide the latency
of slow DRAM accesses by running thousands of concurrent threads. In this project,
I showed that for many applications, the benefits claimed by
the GPU-offloaded systems \emph{do not} arise as much from the GPU hardware as
from the expression of packet processing functionality in a parallel programming
language such as CUDA. I demonstrated that, when similar parallel processing freedom
is given to CPU implementations, they can be better than GPU-offloaded versions
in terms of cost, throughput, and latency.

I first studied the memory subsystem of modern CPUs to understand their DRAM latency-hiding
capabilities and random-access performance characteristics.
By using ideas and experience gleaned from this process, I invented a
mechanism---the G-Opt compile-time source transformation---that can 
automatically hide DRAM latency of parallel data structure lookup code. G-Opt
takes parallel code that processes a batch of data structure lookups, and
emits new code that effectively hides the latency of DRAM accesses for
one lookup by overlapping it with the processing of another.
G-Opt replaces expensive memory accesses in the input code by a primitive called
a ``Prefetch, Save, and Switch'': Instead of issuing a high-latency DRAM \emph{access}
for a packet, the new code issues a \emph{prefetch}, saves the current instruction pointer,
and switches to processing a different packet.  G-Opt is competitive with pre-existing
manual and restrictive latency hiding-techniques, while being automatic and independent
of code structure.  While GPUs indeed outperform our baseline IPv4 router and Layer-2
switch, CPUs are strictly better after optimizing the packet-handling code with G-Opt.
I am working on applying latency-hiding to other packet processing applications including
IPv6 routing, name lookup in Named Data Networking, and Intrusion Detection.

\section*{Applicability of research to Facebook}
Whether over commodity Ethernet or IP, or as a feature in next-generation on-chip NICs,
RDMA is expected to be present in future datacenter networks.  HERD's request-reply
mechanism is a template for maximum performance client-server communication
over RDMA---It can be used to implement traditional services including key-value
stores and RPCs over RDMA, or for prototyping new RDMA features in software.
As Facebook runs some of the largest datacenters in the world, lessons learned from the
HERD project will be relevant to its future key-value stores in particular, and
RDMA-based systems in general.

The G-Opt project shows that DRAM latency-hiding is \emph{simple} and
\emph{widely applicable}. It can be used to improve the efficiency of software packet
processing systems at Facebook, including existing middleboxes, or edge switches in a
future, SDN-based datacenter. It can also increase the performance of existing codes
like hash table lookups and graph traversals.  Further, it challenges the
rising trend of using GPUs (and alternate architectures like FPGAs and
many-core CPUs) in network applications, which will be useful for Facebook in
determining the best architecture for its networking systems.
